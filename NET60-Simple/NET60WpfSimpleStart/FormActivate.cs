﻿using OWork.ICloudEn.ClientContainer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NET60WpfSimpleStart {
    public partial class FormActivate : Form {
        public FormActivate() {
            InitializeComponent();
        }

        /// <summary>
        /// 成功激活的程序集
        /// </summary>
        public Dictionary<string, Assembly> ResultAssemblys { get; set; }

        private async void BtnActivate_Click(object sender, EventArgs e) {
            var activateKey = TxtActivateKey.Text;
            var activateSecret = TxtActivateSecret.Text;
            if (string.IsNullOrEmpty(activateKey)) {
                MessageBox.Show("请输入激活码!");
                TxtActivateKey.Focus();
                TxtActivateKey.SelectAll();
                return;
            }
            if (string.IsNullOrEmpty(activateSecret)) {
                MessageBox.Show("请输入密钥!");
                TxtActivateKey.Focus();
                TxtActivateKey.SelectAll();
                return;
            }

            var activateResult = await CCManager.ActivateAndLoadAssemblys(activateKey, activateSecret);
            if (!activateResult.Success) {
                MessageBox.Show(activateResult.ErrMsg);
                TxtActivateKey.Focus();
                TxtActivateKey.SelectAll();
                return;
            }

            ResultAssemblys = activateResult.Data;
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
