﻿namespace NET60WpfSimpleStart {
    partial class FormActivate {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent() {
            this.BtnActivate = new System.Windows.Forms.Button();
            this.TxtActivateKey = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtActivateSecret = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // BtnActivate
            // 
            this.BtnActivate.Location = new System.Drawing.Point(319, 125);
            this.BtnActivate.Name = "BtnActivate";
            this.BtnActivate.Size = new System.Drawing.Size(76, 42);
            this.BtnActivate.TabIndex = 0;
            this.BtnActivate.Text = "激　活";
            this.BtnActivate.UseVisualStyleBackColor = true;
            this.BtnActivate.Click += new System.EventHandler(this.BtnActivate_Click);
            // 
            // TxtActivateKey
            // 
            this.TxtActivateKey.Location = new System.Drawing.Point(157, 38);
            this.TxtActivateKey.Name = "TxtActivateKey";
            this.TxtActivateKey.Size = new System.Drawing.Size(451, 25);
            this.TxtActivateKey.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(91, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "激活码:";
            // 
            // TxtActivateSecret
            // 
            this.TxtActivateSecret.Location = new System.Drawing.Point(157, 80);
            this.TxtActivateSecret.Name = "TxtActivateSecret";
            this.TxtActivateSecret.Size = new System.Drawing.Size(451, 25);
            this.TxtActivateSecret.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(91, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "密　钥:";
            // 
            // FormStart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(711, 197);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtActivateSecret);
            this.Controls.Add(this.TxtActivateKey);
            this.Controls.Add(this.BtnActivate);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FormStart";
            this.Text = "启动";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnActivate;
        private System.Windows.Forms.TextBox TxtActivateKey;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtActivateSecret;
        private System.Windows.Forms.Label label2;
    }
}

