using System;

namespace NET60WpfSimpleMain {
    public static class StartHelper {

        [OWork.ICloudEn.Inject.ActivateValidation()]
        public static void Run(string[] args) {
            new MainWindow(args).ShowDialog();
        }
    }
}
