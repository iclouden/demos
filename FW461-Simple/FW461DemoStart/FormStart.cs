﻿using OWork.ICloudEn.ClientContainer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OWork.ZVideoEditor.Start {
    public partial class FormStart : Form {
        private string[] AppStartArgs;
        public FormStart(string[] args) {
            AppStartArgs = args;
            InitializeComponent();
        }

        private void FormStart_Load(object sender, EventArgs e) {
            //启动时,直接检查是否激活

            //检测是否有参数传入加载指定加密包
            var loadEPArg = AppStartArgs.FirstOrDefault(arg=>arg.StartsWith("-loadEPKey:"));
            var loadEPKey = loadEPArg?.Substring("-loadEPKey:".Length);

            CCManager.Init(
                "e95dc6a30590428e96ae82bb9b991bd1", 
                "<RSAKeyValue><Modulus>pwwRYxGvkB0qyPcFNVbyckLKjvR2gaCfpj2Zmco/34aSXxtYra5cBHwdM1n6iDWWzKVfbopuihQXAGdy/8KppUE0FHKNQweTUMCJsbrZfxa1zkfIGoSeKD0jOCR8DrIt/XfOSNSppwJfMydVG3HdxOyRsaepBX1V17bHIEZbXdk=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>",
                loadEPKey);

            var loadSuccess = false;
            if (AppStartArgs.Contains("-debug")) {
                //调试参数,直接加载同目录下的dll
                var dllDir = Path.GetDirectoryName(typeof(FormStart).Assembly.Location);
                var assembly = Assembly.LoadFile(Path.Combine(dllDir, "FW461DemoMainUI.dll"));
                CCManager.EntryPointAssembly = assembly;
                loadSuccess = true;
            } else {
                //正常加载加密程序集
                if (CCManager.HasActivate()) {
                    var loadResult = CCManager.LoadAssemblys();
                    if (loadResult.Success) {
                        //加载成功, 一般是 签名没通过/过期 导致加载失败
                        loadSuccess = true;
                    }
                }
            }

            if (loadSuccess) {
                //加载成功,则隐藏启动窗口
                Hide();
            } else {
                //没加载成功,则需要弹出激活窗口
                var win = new FormActivate();
                if (win.ShowDialog() != DialogResult.OK || win.ResultAssemblys == null) {
                    //不激活,则退出程序
                    Close();
                    return;
                }
            }

            var startType = CCManager.EntryPointAssembly.GetType("FW461DemoMainUI.StartHelper");
            var runMethod = startType.GetMethod("Run");
            try {
                //需要运行方法hold住,作为主线程使用!
                runMethod.Invoke(null, new object[] { AppStartArgs });
            } catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
            //到这里说明主线程已经停止,则正常退出程序
            Close();
        }
    }
}
