﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FW461DemoBLL {
    /// <summary>
    /// 模拟一些视频的关键操作,需要加密的方法
    /// </summary>
    public static class BVideo {

        /// <summary>
        /// 绘制水印图片
        /// </summary>
        /// <param name="g">绘制对象</param>
        /// <param name="watermark"></param>
        [OWork.ICloudEn.Inject.ActivateValidation()]
        public static void DrawWatermark(Graphics g, Bitmap watermark) {
            g.DrawImage(watermark, 0, 0);
        }

    }
}
