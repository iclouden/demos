﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FW461DemoUtility {
    /// <summary>
    /// 模拟一些通用方法,可以不用加密的
    /// </summary>
    public static class Utilitys {

        /// <summary>
        /// 根据容器的尺寸,计算出顶格并居中的尺寸和位置
        /// </summary>
        /// <param name="panelWidth"></param>
        /// <param name="panelHeight"></param>
        /// <param name="rawWidth"></param>
        /// <param name="rawHeight"></param>
        /// <param name="setWidth"></param>
        /// <param name="setHeight"></param>
        /// <param name="setTop"></param>
        /// <param name="setLeft"></param>
        public static void AdaptSize(int panelWidth, int panelHeight, int rawWidth, int rawHeight,
            out int setWidth, out int setHeight, out int setTop, out int setLeft) {
            var rawWH = (double)rawWidth / rawHeight;
            var panelWH = (double)panelWidth / panelHeight;
            if (rawWH > panelWH) {
                //如果原始图片的宽比, 比容器的宽比大, 则使用容器的宽进行缩放
                setWidth = panelWidth;
                setHeight = (int)(setWidth / rawWH);
                setLeft = 0;
                setTop = (panelHeight - setHeight) / 2;
            } else {
                //否则使用容器的高度进行缩放
                setHeight = panelHeight;
                setWidth= (int)(setHeight * rawWH);
                setTop = 0;
                setLeft = (panelWidth - setWidth) / 2;
            }
        }

    }
}
