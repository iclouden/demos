﻿namespace FW461DemoMainUI {
    partial class FormMain {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent() {
            this.label2 = new System.Windows.Forms.Label();
            this.TxtOpenFile = new System.Windows.Forms.TextBox();
            this.BtnOpenFile = new System.Windows.Forms.Button();
            this.PanelPlayer = new System.Windows.Forms.Panel();
            this.VideoSourcePlayer = new AForge.Controls.VideoSourcePlayer();
            this.PanelPlayer.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(48, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "label1";
            // 
            // TxtOpenFile
            // 
            this.TxtOpenFile.Location = new System.Drawing.Point(118, 35);
            this.TxtOpenFile.Name = "TxtOpenFile";
            this.TxtOpenFile.Size = new System.Drawing.Size(702, 25);
            this.TxtOpenFile.TabIndex = 2;
            this.TxtOpenFile.Text = "E:\\Users\\zum\\Desktop\\我的歌声里[超清版].avi";
            // 
            // BtnOpenFile
            // 
            this.BtnOpenFile.Location = new System.Drawing.Point(826, 35);
            this.BtnOpenFile.Name = "BtnOpenFile";
            this.BtnOpenFile.Size = new System.Drawing.Size(75, 25);
            this.BtnOpenFile.TabIndex = 0;
            this.BtnOpenFile.Text = "加载";
            this.BtnOpenFile.UseVisualStyleBackColor = true;
            this.BtnOpenFile.Click += new System.EventHandler(this.BtnOpenFile_Click);
            // 
            // PanelPlayer
            // 
            this.PanelPlayer.Controls.Add(this.VideoSourcePlayer);
            this.PanelPlayer.Location = new System.Drawing.Point(51, 85);
            this.PanelPlayer.Name = "PanelPlayer";
            this.PanelPlayer.Size = new System.Drawing.Size(884, 359);
            this.PanelPlayer.TabIndex = 3;
            // 
            // VideoSourcePlayer
            // 
            this.VideoSourcePlayer.Location = new System.Drawing.Point(67, 14);
            this.VideoSourcePlayer.Name = "VideoSourcePlayer";
            this.VideoSourcePlayer.Size = new System.Drawing.Size(747, 325);
            this.VideoSourcePlayer.TabIndex = 0;
            this.VideoSourcePlayer.Text = "videoSourcePlayer1";
            this.VideoSourcePlayer.VideoSource = null;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(966, 530);
            this.Controls.Add(this.PanelPlayer);
            this.Controls.Add(this.TxtOpenFile);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.BtnOpenFile);
            this.Name = "FormMain";
            this.Text = "版本2";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormMain_FormClosed);
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.PanelPlayer.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxtOpenFile;
        private System.Windows.Forms.Button BtnOpenFile;
        private System.Windows.Forms.Panel PanelPlayer;
        private AForge.Controls.VideoSourcePlayer VideoSourcePlayer;
    }
}

