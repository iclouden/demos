﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FW461DemoMainUI {
    public static class StartHelper {
        [OWork.ICloudEn.Inject.ActivateValidation()]
        public static void Run(string[] args) {
            new FormMain(args).ShowDialog();
        }
    }
}
